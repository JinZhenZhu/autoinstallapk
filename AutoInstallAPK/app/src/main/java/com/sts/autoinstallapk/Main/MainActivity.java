package com.sts.autoinstallapk.Main;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.sts.autoinstallapk.Base.BaseActivity;
import com.sts.autoinstallapk.Common.Constant;
import com.sts.autoinstallapk.R;
import com.sts.autoinstallapk.Utils.ApkFinder;
import com.sts.autoinstallapk.Utils.ApkItem;

import org.bunnyblue.autoinstaller.util.AutoInstallerContext;
import org.bunnyblue.autoinstaller.util.IApkInstaller;
import org.bunnyblue.autoinstaller.util.InstallerUtils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class MainActivity extends BaseActivity {

    Button btnDownload;
    private ProgressBar progressBar;

    private TextView txvSpeed, txvFileName;

    private String filePath;
    ApkPickerReceiver mApkPickerReceiver;
    SharedPreferences mSharedPreferences;
    LinkedList<ApkItem> apkPaths;
    String dirPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initial();

        loadLayout();
    }

    private void loadLayout() {
        btnDownload = (Button)findViewById(R.id.btnDownload);
        btnDownload.setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txvSpeed = (TextView)findViewById(R.id.txvSpeed);
        txvFileName = (TextView)findViewById(R.id.txvFileName);
    }

    private void initial() {
        dirPath = getSharedPreferences("config", Context.MODE_PRIVATE).getString("dir", "");
        apkPaths = new LinkedList<ApkItem>();

        mSharedPreferences = getSharedPreferences("config", Context.MODE_PRIVATE);

        filePath = FileDownloadUtils.getDefaultSaveRootPath() + File.separator + "tmpdir1" + File.separator + Constant.FILE_NAME;

        mApkPickerReceiver = new ApkPickerReceiver();
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(ApkPickerReceiver.ACTION_END_PICK);
        mFilter.addAction(ApkPickerReceiver.ACTION_PICKED);
        mFilter.addAction(ApkPickerReceiver.ACTION_START_PICK);
        registerReceiver(mApkPickerReceiver, mFilter);

        mSharedPreferences.edit().putString("dir", filePath).commit();
    }

    private BaseDownloadTask downloadAPK() {
        CustomViewHolderForDownload tag;
        boolean isDir = false;

        String url = "http://54.68.180.131/Transform_11_25.apk";
        tag = new CustomViewHolderForDownload(new WeakReference<>(this), progressBar, null, txvSpeed, 1);
        String path = filePath;
        tag.setFilenameTv(txvFileName);

        return FileDownloader.getImpl().create(url)
                .setPath(path, isDir)
                .setCallbackProgressTimes(300)
                .setMinIntervalUpdateSpeed(400)
                .setTag(tag)
                .setListener(new FileDownloadSampleListener() {

                    @Override
                    protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        super.pending(task, soFarBytes, totalBytes);
                        ((CustomViewHolderForDownload) task.getTag()).updatePending(task);
                    }

                    @Override
                    protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        super.progress(task, soFarBytes, totalBytes);
                        ((CustomViewHolderForDownload) task.getTag()).updateProgress(soFarBytes, totalBytes,
                                task.getSpeed());
                    }

                    @Override
                    protected void error(BaseDownloadTask task, Throwable e) {
                        super.error(task, e);
                        ((CustomViewHolderForDownload) task.getTag()).updateError(e, task.getSpeed());
                    }

                    @Override
                    protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
                        super.connected(task, etag, isContinue, soFarBytes, totalBytes);
                        ((CustomViewHolderForDownload) task.getTag()).updateConnected(etag, task.getFilename());
                    }

                    @Override
                    protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        super.paused(task, soFarBytes, totalBytes);
                        ((CustomViewHolderForDownload) task.getTag()).updatePaused(task.getSpeed());
                    }

                    @Override
                    protected void completed(BaseDownloadTask task) {
                        super.completed(task);
                        ((CustomViewHolderForDownload) task.getTag()).updateCompleted(task);
                        installALl();
                    }

                    @Override
                    protected void warn(BaseDownloadTask task) {
                        super.warn(task);
                        ((CustomViewHolderForDownload) task.getTag()).updateWarn();
                    }
                });
    }

    private void installALl() {

        IApkInstaller m = new IApkInstaller() {

            @Override
            public void startInstall(String name, String path) {}

            @Override
            public void endInstall(String name, String path) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        };
        AutoInstallerContext.setApkInstallMonitor(m);
        m.endInstall("", "");
    }

    @Override
    protected void onResume() {
        super.onResume();
/*        if (!InstallerUtils.isEnableAutoInstall()) {
            Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
            startActivityForResult(intent, 0);
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnDownload:
                downloadAPK().start();
                showBottomToast("Downloading");
                break;
        }
    }

    private static class CustomViewHolderForDownload {
        private ProgressBar pb;
        private TextView detailTv;
        private TextView speedTv;
        private int position;
        private TextView filenameTv;

        private WeakReference<MainActivity> weakReferenceContext;

        public CustomViewHolderForDownload(WeakReference<MainActivity> weakReferenceContext,
                                           final ProgressBar pb, final TextView detailTv, final TextView speedTv,
                                           final int position) {
            this.weakReferenceContext = weakReferenceContext;
            this.pb = pb;
            this.detailTv = detailTv;
            this.position = position;
            this.speedTv = speedTv;
        }

        public void setFilenameTv(TextView filenameTv) {
            this.filenameTv = filenameTv;
        }

        private void updateSpeed(int speed) {
            speedTv.setText(String.format("%dKB/s", speed));
        }

        public void updateProgress(final int sofar, final int total, final int speed) {
            if (total == -1) {
                // chunked transfer encoding data
                pb.setIndeterminate(true);
            } else {
                pb.setMax(total);
                pb.setProgress(sofar);
            }

            updateSpeed(speed);

            if (detailTv != null) {
                detailTv.setText(String.format("sofar: %d total: %d", sofar, total));
            }
        }

        public void updatePending(BaseDownloadTask task) {
            if (filenameTv != null) {
                filenameTv.setText(task.getFilename());
            }
        }

        public void updatePaused(final int speed) {
            toast(String.format("paused %d", position));
            updateSpeed(speed);
            pb.setIndeterminate(false);
        }

        public void updateConnected(String etag, String filename) {
            if (filenameTv != null) {
                filenameTv.setText(filename);
            }
        }

        public void updateWarn() {
            toast(String.format("warn %d", position));
            pb.setIndeterminate(false);
        }

        public void updateError(final Throwable ex, final int speed) {
            toast(String.format("error %d %s", position, ex));
            updateSpeed(speed);
            pb.setIndeterminate(false);
            ex.printStackTrace();
        }

        public void updateCompleted(final BaseDownloadTask task) {

            toast(String.format("completed %d %s", position, task.getTargetFilePath()));

            if (detailTv != null) {
                detailTv.setText(String.format("sofar: %d total: %d",
                        task.getSmallFileSoFarBytes(), task.getSmallFileTotalBytes()));
            }

            updateSpeed(task.getSpeed());
            pb.setIndeterminate(false);
            pb.setMax(task.getSmallFileTotalBytes());
            pb.setProgress(task.getSmallFileSoFarBytes());
        }

        private void toast(final String msg) {
            if (this.weakReferenceContext != null && this.weakReferenceContext.get() != null) {
                Snackbar.make(weakReferenceContext.get().btnDownload, msg, Snackbar.LENGTH_LONG).show();
            }
        }

    }

    class ApkPickerReceiver extends BroadcastReceiver {
        public static final String ACTION_START_PICK = "ACTION_ApkPickerReceiver_START_PICK";
        public static final String ACTION_PICKED = "ACTION_ApkPickerReceiver_PICKED";
        public static final String ACTION_END_PICK = "ACTION_ApkPickerReceiver_END";

        public ApkPickerReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_PICKED)) {
                Log.d("aaaaa", "aaaaaa");
            } else if (intent.getAction().equals(ACTION_END_PICK)) {
                Log.d("aaaaa", "bbbbbb");
            }
        }
    }
}
